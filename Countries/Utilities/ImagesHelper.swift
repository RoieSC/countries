//
//  ImagesHelper.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 23/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit
import SDWebImageSVGCoder
import SDWebImageSVGKitPlugin

class CountryImageHelper {
    
    class func loadImage(country: Country, isBigImage: Bool = false, completion:@escaping (Country, UIImage?) -> ()) {
        
        let defaultImage = UIImage(named: isBigImage ? "flagsBig" : "flags")
        guard let flagURLPath = country.flagURLPath, !flagURLPath.isEmpty else {
            completion(country, defaultImage)
            return
        }
        
        let flagURL = URL(string: flagURLPath)
        let cacheKey = isBigImage ? "big_\(String(describing: flagURL?.lastPathComponent))" : flagURL?.lastPathComponent
        
        SDImageCache.shared.queryCacheOperation(forKey: cacheKey) { (image, data, type) in
            if let image = image {
                completion(country, image)
            } else {
                SDWebImageDownloader.shared.downloadImage(with: flagURL) { (image, data, error, finished) in
                    if let image = image, finished && !self.isCorruptImage(lastPathComponent: flagURL!.lastPathComponent) {
                        if isBigImage {
                            SDImageCache.shared.store(image, forKey: cacheKey, completion: nil)
                            completion(country, image)
                        } else {
                            let _image = image.resized(maxSize: CGSize(width: 144, height: 96))
                            SDImageCache.shared.store(_image, forKey: cacheKey, completion: nil)
                            completion(country, _image)
                        }
                    } else {
                        completion(country, defaultImage)
                    }
                }
            }
        }
    }
    
    private class func isCorruptImage(lastPathComponent: String) -> Bool {
        let corruptImagesLastPathComponent = ["ecu.svg", "nic.svg"]
        for pathComponent in corruptImagesLastPathComponent {
            if pathComponent == lastPathComponent {
                return true
            }
        }
        return false
    }
}

extension UIImage {
    public func resized(maxSize: CGSize) -> UIImage? {
        let imageSize = self.size
        guard imageSize.height > 0, imageSize.width > 0 else { return nil }
        
        let ratio = min(maxSize.width/imageSize.width, maxSize.height/imageSize.height)
        let newSize = CGSize(width: imageSize.width*ratio, height: imageSize.height*ratio)
        
        let renderer = UIGraphicsImageRenderer(size: newSize)
        return renderer.image(actions: { (ctx) in
            self.draw(in: CGRect(origin: .zero, size: newSize))
        })
    }
}
