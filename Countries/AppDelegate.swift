//
//  AppDelegate.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 22/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit
import SDWebImageSVGCoder
import SDWebImageSVGKitPlugin

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
        
        //SDImageSVGCoder
        if #available(iOS 13, *) {
            SDImageCodersManager.shared.addCoder(SDImageSVGCoder.shared)
        } else {
            SDImageCodersManager.shared.addCoder(SDImageSVGKCoder.shared)
        }
        
        return true
    }

}

