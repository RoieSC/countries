//
//  Country.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 22/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import Foundation

class Country: Decodable {

    var name: String?
    var flagURLPath: String?
    var id: String?
    var isFavorite = false
    
    init(id:String, name: String, flagURLPath: String, isFavorite: Bool) {
        self.id = id
        self.name = name
        self.flagURLPath = flagURLPath
        self.isFavorite = isFavorite
    }

    enum CodingKeys: String, CodingKey {
        case id = "numericCode"
        case name
        case flagURLPath = "flag"
    }
}

