//
//  Country+SQLiteDatabase.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 24/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import Foundation
import SQLite3

extension Country: SQLTable {
    static var createStatement: String {
        return """
        CREATE TABLE IF NOT EXISTS Country(
        Id TEXT PRIMARY KEY,
        name TEXT,
        flagURLPath TEXT
        );
        """
    }
}

extension SQLiteDatabase {
    func insertCountry(country: Country) throws {
        let insertSql = "INSERT INTO Country (Id, name, flagURLPath) VALUES (?, ?, ?);"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        let name: NSString = country.name! as NSString
        let id: NSString = country.id! as NSString
        let flagURLPath: NSString = country.flagURLPath! as NSString
        guard
            sqlite3_bind_text(insertStatement, 1, id.utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(insertStatement, 3, flagURLPath.utf8String, -1, nil) == SQLITE_OK
            else {
                throw SQLiteError.Bind(message: errorMessage)
        }
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage)
        }
        print("Successfully inserted row.")
    }
    
    func readCountries() -> [Country]? {
        let querySql = "SELECT * FROM Country;"
        guard let queryStatement = try? prepareStatement(sql: querySql) else {
            return nil
        }
        defer {
            sqlite3_finalize(queryStatement)
        }
        var countries = [Country]()
        while sqlite3_step(queryStatement) == SQLITE_ROW {
            let id = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
            let name = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
            let flagURLPath = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
            countries.append(Country(id: id, name: name, flagURLPath: flagURLPath, isFavorite: true))
        }
        return countries
    }
}
