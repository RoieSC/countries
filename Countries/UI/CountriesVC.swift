//
//  CountriesVC.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 22/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit

class CountriesVC: UIViewController {
    
    var countriesManager: CountriesManager = CountriesManager.shared
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showCountriesBtn: UIButton!
    
    //Search
    let searchController = UISearchController(searchResultsController: nil)
    var filteredCountries = [Country]()
    var isFiltering: Bool {
      return searchController.isActive && !(searchController.searchBar.text?.isEmpty ?? true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchController()
    }

    //MARK: Actions
    @IBAction func showCountriesButtonPressed(_ sender: Any) {
        showCountriesBtn.setTitle("Loading...", for: .normal)
        showCountriesBtn.isUserInteractionEnabled = false
        countriesManager.fetchCountries { (sucsses) in
            if sucsses {
                self.showCountriesBtn.isHidden = true
                self.tableView.reloadData()
                self.tableView.isHidden = false
                self.searchController.searchBar.isHidden = false
            }
        }
    }
    
    //MARK: Utilities
    func getCountry(indexPath: IndexPath) -> Country {
        var country: Country
        if isFiltering {                                        // Searching
            country = filteredCountries[indexPath.row]
        } else if hasFavorites() {                              // Has favorites
            switch indexPath.section {
            case 0:
                country = countriesManager.favoriteCountries![indexPath.row]
            default:
                country = countriesManager.notFavoriteCountries[indexPath.row]
            }
        } else {                                                // No favorites
            country = countriesManager.allCountries[indexPath.row]
        }
        return country
    }
    
    func hasFavorites() -> Bool {
        return countriesManager.numOfFavoriteCountries > 0
    }
}

extension CountriesVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering {
            return 1
        }
        return hasFavorites() ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {                                // Searching
            return filteredCountries.count
        }
        
        if hasFavorites() {                             // Has favorites
            switch section {
            case 0:
                return countriesManager.numOfFavoriteCountries
            default:
                return countriesManager.notFavoriteCountries.count
            }
        }
        
        return countriesManager.allCountries.count      // No favorites
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryCell
        let country = getCountry(indexPath: indexPath)
        cell.setCountry(country: country)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering {
            return "Search Results"
        }
        
        if hasFavorites() {
            switch section {
            case 0:
                return "Favorite Countries"
            default:
                return "All other Countries"
            }
        }
        return "All Countries"
    }
}

extension CountriesVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = getCountry(indexPath: indexPath)
        let storyboard = UIStoryboard(name: "CountryVC", bundle: .main)
        if let countryVC = storyboard.instantiateViewController(withIdentifier: "CountryVC") as? CountryVC {
            countryVC.country = country
            self.navigationController?.pushViewController(countryVC, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CountriesVC: UISearchResultsUpdating {
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Country"
        searchController.searchBar.isHidden = true
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification, object: nil, queue: .main) { (notification) in
            self.handleKeyboard(notification: notification)
        }
        notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { (notification) in
            self.handleKeyboard(notification: notification)
        }
    }
    
    //Changing tableView.contentInset according to keyboard visibility
    func handleKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredCountries = countriesManager.allCountries.filter { (country: Country) -> Bool in
            return (country.name?.lowercased().contains(searchText.lowercased()) ?? false)
        }
        tableView.reloadData()
    }
    
    //UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
