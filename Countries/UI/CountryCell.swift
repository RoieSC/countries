//
//  CountryCell.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 22/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    @IBOutlet weak var flagIV: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    var country: Country?
    
    func setCountry(country: Country) {
        self.country = country
        nameLabel.text = country.name
        let defaultImage = UIImage(named: "flags")
        flagIV.image = defaultImage
        CountryImageHelper.loadImage(country: country) { (country, image) in
            if let image = image, self.country?.id == country.id {
                self.flagIV.image = image
            }
        }
    }
}
