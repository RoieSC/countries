//
//  CountryVC.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 23/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit

class CountryVC: UIViewController {
    
    var country: Country?
    @IBOutlet weak var flagIV: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let country = country else { return }
        self.navigationItem.title = country.name
        
        activityIndicator.startAnimating()
        CountryImageHelper.loadImage(country: country, isBigImage: true) { [weak self] (country, image)  in
            if let image = image, self?.country?.id == country.id {
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.flagIV.image = image
            }
        }
        
        CountriesManager.shared.addFavoriteCounrtyToDB(country: country)
    }
    
}
