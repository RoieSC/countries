//
//  CountriesManager.swift
//  Countries
//
//  Created by Roie Shimon Cohen on 22/08/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import Foundation
import Alamofire

class CountriesManager {
    
    static let shared = CountriesManager()
    static let countries_api_url_path = "https://restcountries.eu/rest/v2/all"
    var db: SQLiteDatabase?
    var allCountries = [Country]()
    var favoriteCountries: [Country]?
    var notFavoriteCountries = [Country]()
    var numOfFavoriteCountries: Int {
        guard let favoriteCountries = favoriteCountries else { return 0 }
        return favoriteCountries.count
    }
    
    init() {
        initializeDB()
        favoriteCountries = getFavoriteCountriesFromDB()
    }
    
    //MARK: Networking
    func fetchCountries(completion:@escaping (Bool) -> ()) {
        AF.request(CountriesManager.countries_api_url_path).validate().responseDecodable(of: [Country].self) { (response) in
            guard let countries = response.value else {
                completion(false)
                return
            }
            self.allCountries = countries
            self.setNotFavoriteCountries()
            completion(true)
        }
    }
    
    //MARK: SQLiteDatabase
    func initializeDB() {
        //Create SQLiteDatabase
        do {
            if let db = try? SQLiteDatabase.open(path: "FavoriteCountries.sqlite") {
                self.db = db
            } else {
                print("Unable to open database.")
            }
        }
        
        //Create Table
        do {
            try db?.createTable(table: Country.self)
        } catch {
            print("Error create table: \(db?.errorMessage ?? "")")
        }
    }
    
    func addFavoriteCounrtyToDB(country: Country) {
        do {
            try db?.insertCountry(country: country)
        } catch {
            print("Error insert country: \(db?.errorMessage ?? "")")
        }
    }
    
    func getFavoriteCountriesFromDB() -> [Country]? {
        return db?.readCountries()
    }
    
    //MARK: Utilities
    func setNotFavoriteCountries() {
        self.allCountries.forEach { (country) in
            country.setIsFavorite()
        }
        
        notFavoriteCountries = allCountries.filter { country in
            return !country.isFavorite
        }
    }
}

extension Country {
    func setIsFavorite() {
        guard let favoriteCountries = CountriesManager.shared.favoriteCountries else {
            self.isFavorite = false
            return
        }
        for country in favoriteCountries {
            if self.id == country.id {
                self.isFavorite = true
                return
            }
        }
        self.isFavorite = false
    }
}
